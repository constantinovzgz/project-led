var express = require('express');
var router = express.Router();
var Host = require('../models/ips');
var getIP = require('ipware')().get_ip;
var Gpio = require('onoff').Gpio;
var LED2 = new Gpio(27, 'out');
var LED1 = new Gpio(17, 'out');

router.get('/', function(req, res, next){

	res.render('index', {title : 'Equipo patron'});

});

router.get('/led/:id:/:id2', function(req, res, next){

	var id = req.params.id;
	var id2 = req.params.id2;



if(id == '1')
{

	if(id2 == '1')
	{

		LED1.writesync(1);

	}
	else
	{

		LED1.writesync(0);

	}

}
else
{

	if(id2 == '1')
	{

		LED2.writesync(1);

	}
	else
	{

		LED2.writesync(0);

	}

}

var flag = '';
if(id2 == '0')
{

	flag = 'Apagado';

}
else
{

	flag = 'Prendido';

};


var ipInfo = getIP(req);
var clienteIp = ipInfo.clientIp;
var ip = clientIp.substring(7, 21);
var json = {"host":ip, "led":id, "status":flag};

Host.build(json).save();
res.redirect('/');

});


module.exports = router;